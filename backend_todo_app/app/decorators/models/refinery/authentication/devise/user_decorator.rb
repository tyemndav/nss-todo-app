Refinery::Authentication::Devise::User.class_eval do

  has_many :cards, :class_name => '::Refinery::Cards::Card'
  has_many :project_tables, :class_name => '::Refinery::ProjectTables::ProjectTable'
  has_many :project_developers, :class_name => '::Refinery::ProjectDevelopers::ProjectDeveloper'
  has_many :project_tables, :class_name => '::Refinery::ProjectTables::ProjectTable', :through => :project_developers
  has_many :developers, :class_name => '::Refinery::Developers::Developer'
  has_many :tasks, :class_name => '::Refinery::Cards::Card', :through => :developers


  def superadmin?
    username == "superadmin"
  end

  def name
    full_name.blank? ? username : full_name
  end
end