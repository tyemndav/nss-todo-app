class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :get_all_users
  before_action :find_all_project_tables



  def get_all_users
    @users = ::Refinery::Authentication::Devise::User.all
  end

  def find_all_project_tables
    @project_tables = ::Refinery::ProjectTables::ProjectTable.order('position ASC')
  end

  protected
  def after_sign_in_path_for(resource)
    refinery.root_path
  end
end
