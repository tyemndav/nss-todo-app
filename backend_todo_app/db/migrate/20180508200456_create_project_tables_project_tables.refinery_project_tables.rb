# This migration comes from refinery_project_tables (originally 1)
class CreateProjectTablesProjectTables < ActiveRecord::Migration[5.1]

  def up
    create_table :refinery_project_tables do |t|
      t.string :title
      t.string :color
      t.integer :user_id
      t.integer :position

      t.timestamps
    end

  end

  def down
    if defined?(::Refinery::UserPlugin)
      ::Refinery::UserPlugin.destroy_all({:name => "refinerycms-project_tables"})
    end

    if defined?(::Refinery::Page)
      ::Refinery::Page.delete_all({:link_url => "/project_tables/project_tables"})
    end

    drop_table :refinery_project_tables

  end

end
