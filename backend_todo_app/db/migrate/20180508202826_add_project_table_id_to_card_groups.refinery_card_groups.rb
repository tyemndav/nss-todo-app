# This migration comes from refinery_card_groups (originally 2)
class AddProjectTableIdToCardGroups < ActiveRecord::Migration[5.1]

  def change
    add_column :refinery_card_groups, :project_table_id, :integer
  end

end
