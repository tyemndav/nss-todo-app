# This migration comes from refinery_project_tables (originally 2)
class CreateProjectDevelopersProjectDevelopers < ActiveRecord::Migration[5.1]

  def up
    create_table :refinery_project_developers do |t|
      t.integer :user_id
      t.integer :project_table_id
      t.integer :position
      t.timestamps
    end
  end

  def down

    drop_table :refinery_project_developers

  end

end
