# This migration comes from refinery_card_groups (originally 3)
class RemoveUserIdFromCardGroups < ActiveRecord::Migration[5.1]

  def change
    remove_column :refinery_card_groups, :user_id
  end

end
