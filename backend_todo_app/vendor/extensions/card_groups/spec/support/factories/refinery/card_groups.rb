
FactoryGirl.define do
  factory :card_group, :class => Refinery::CardGroups::CardGroup do
    sequence(:title) { |n| "refinery#{n}" }
  end
end

