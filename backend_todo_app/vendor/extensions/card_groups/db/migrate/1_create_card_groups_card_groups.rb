class CreateCardGroupsCardGroups < ActiveRecord::Migration[5.1]

  def up
    create_table :refinery_card_groups do |t|
      t.string :title
      t.integer :user_id
      t.integer :position

      t.timestamps
    end

  end

  def down
    if defined?(::Refinery::UserPlugin)
      ::Refinery::UserPlugin.destroy_all({:name => "refinerycms-card_groups"})
    end

    if defined?(::Refinery::Page)
      ::Refinery::Page.delete_all({:link_url => "/card_groups/card_groups"})
    end

    drop_table :refinery_card_groups

  end

end
