class RemoveUserIdFromCardGroups < ActiveRecord::Migration[5.1]

  def change
    remove_column :refinery_card_groups, :user_id
  end

end
