# Card Groups extension for Refinery CMS.

## How to build this extension as a gem (not required)

    cd vendor/extensions/card_groups
    gem build refinerycms-card_groups.gemspec
    gem install refinerycms-card_groups.gem

    # Sign up for a https://rubygems.org/ account and publish the gem
    gem push refinerycms-card_groups.gem
