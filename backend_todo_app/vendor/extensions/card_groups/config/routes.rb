Refinery::Core::Engine.routes.draw do

  # Frontend routes
  namespace :card_groups do
    resources :card_groups, :path => '', :only => [:index, :show]
  end

  # Admin routes
  namespace :card_groups, :path => '' do
    namespace :admin, :path => Refinery::Core.backend_route do
      resources :card_groups, :except => :show do
        collection do
          post :update_positions
        end
      end
    end
  end

end
