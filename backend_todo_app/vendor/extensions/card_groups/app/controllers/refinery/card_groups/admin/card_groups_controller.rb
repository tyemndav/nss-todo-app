module Refinery
  module CardGroups
    module Admin
      class CardGroupsController < ::Refinery::AdminController

        crudify :'refinery/card_groups/card_group'

        private

        # Only allow a trusted parameter "white list" through.
        def card_group_params
          params.require(:card_group).permit!#(:title, :user_id)
        end
      end
    end
  end
end
