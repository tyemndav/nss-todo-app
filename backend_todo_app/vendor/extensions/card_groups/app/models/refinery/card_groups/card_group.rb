module Refinery
  module CardGroups
    class CardGroup < Refinery::Core::BaseModel
      self.table_name = 'refinery_card_groups'


      validates :title, :presence => true
      belongs_to :project_table, :class_name => '::Refinery::ProjectTables::ProjectTable'
      has_many :cards, :class_name => '::Refinery::Cards::Card'

      # To enable admin searching, add acts_as_indexed on searchable fields, for example:
      #
      #   acts_as_indexed :fields => [:title]

    end
  end
end
