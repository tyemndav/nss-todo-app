module Refinery
  module CardGroups
    class Engine < Rails::Engine
      extend Refinery::Engine
      isolate_namespace Refinery::CardGroups

      engine_name :refinery_card_groups

      before_inclusion do
        Refinery::Plugin.register do |plugin|
          plugin.name = "card_groups"
          plugin.url = proc { Refinery::Core::Engine.routes.url_helpers.card_groups_admin_card_groups_path }
          plugin.pathname = root
          
        end
      end

      config.after_initialize do
        Refinery.register_extension(Refinery::CardGroups)
      end
    end
  end
end
