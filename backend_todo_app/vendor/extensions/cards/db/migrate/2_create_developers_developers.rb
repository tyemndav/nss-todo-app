class CreateDevelopersDevelopers < ActiveRecord::Migration[5.1]

    def up
      create_table :refinery_developers do |t|
        t.integer :user_id
        t.integer :card_id
        t.integer :position
        t.timestamps
      end
    end

    def down

      drop_table :refinery_developers

    end

end
