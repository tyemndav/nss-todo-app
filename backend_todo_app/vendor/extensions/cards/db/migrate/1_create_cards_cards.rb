class CreateCardsCards < ActiveRecord::Migration[5.1]

  def up
    create_table :refinery_cards do |t|
      t.string :title
      t.integer :user_id
      t.string :state
      t.text :note
      t.datetime :date
      t.integer :card_group_id
      t.integer :position

      t.timestamps
    end

  end

  def down
    if defined?(::Refinery::UserPlugin)
      ::Refinery::UserPlugin.destroy_all({:name => "refinerycms-cards"})
    end

    if defined?(::Refinery::Page)
      ::Refinery::Page.delete_all({:link_url => "/cards/cards"})
    end

    drop_table :refinery_cards

  end

end
