module Refinery
  module Cards
    module Admin
      class CardsController < ::Refinery::AdminController

        before_action :get_all_users

        crudify :'refinery/cards/card'

        def get_all_users
          @users = ::Refinery::Authentication::Devise::User.all
        end


        private

        # Only allow a trusted parameter "white list" through.
        def card_params
          params.require(:card).permit!#(:title, :user_id, :state, :note, :date, :card_group_id)
        end
      end
    end
  end
end
