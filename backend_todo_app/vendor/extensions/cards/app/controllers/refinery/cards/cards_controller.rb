module Refinery
  module Cards
    class CardsController < ::ApplicationController

      before_action :find_all_cards
      before_action :find_page

      def index
        # you can use meta fields from your model instead (e.g. browser_title)
        # by swapping @page for @card in the line below:
        @project_table = Refinery::ProjectTables::ProjectTable.find_by_id params[:project_table_id]
        @edit_card = false
        present(@page)
      end

      def show
        @card = Card.find(params[:id])
        @project_table = Refinery::ProjectTables::ProjectTable.find_by_id params[:project_table_id]
        @edit_card = true
        # you can use meta fields from your model instead (e.g. browser_title)
        # by swapping @page for @card in the line below:
        present(@page)
      end

      def create
        card_params
        card_attributes = params[:card]
        card = Refinery::Cards::Card.new card_attributes
        card.save!
        redirect_to refinery.root_path
      end

      def update
        card_params
        card_attributes = params[:card]
        card_id = params[:card_id]
        card = Refinery::Cards::Card.find_by_id card_id
        card.update card_attributes
        card.save!
        redirect_to refinery.root_path
      end

      def my_own_destroy
        card_params
        card = Refinery::Cards::Card.find_by_id card_params[:id]
        card.developers.destroy_all
        card.destroy!
        redirect_to refinery.root_path
      end

    protected

      def find_all_cards
        @cards = Card.order('position ASC')
      end

      def find_page
        @page = ::Refinery::Page.where(:link_url => "/cards").first
      end

     private

      def card_params
        params.require(:card).permit!#(:title, :color, :user_id)
      end

    end
  end
end
