module Refinery
  module Developers
    class Developer < Refinery::Core::BaseModel
      self.table_name = 'refinery_developers'

      belongs_to :user, :class_name => '::Refinery::Authentication::Devise::User'
      belongs_to :card, :class_name => '::Refinery::Cards::Card'

      # To enable admin searching, add acts_as_indexed on searchable fields, for example:
      #
      #   acts_as_indexed :fields => [:title]

    end
  end
end
