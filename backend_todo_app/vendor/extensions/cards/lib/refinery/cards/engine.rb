module Refinery
  module Cards
    class Engine < Rails::Engine
      extend Refinery::Engine
      isolate_namespace Refinery::Cards

      engine_name :refinery_cards

      before_inclusion do
        Refinery::Plugin.register do |plugin|
          plugin.name = "cards"
          plugin.url = proc { Refinery::Core::Engine.routes.url_helpers.cards_admin_cards_path }
          plugin.pathname = root
          
        end
      end

      config.after_initialize do
        Refinery.register_extension(Refinery::Cards)
      end
    end
  end
end
