
FactoryGirl.define do
  factory :card, :class => Refinery::Cards::Card do
    sequence(:title) { |n| "refinery#{n}" }
  end
end

