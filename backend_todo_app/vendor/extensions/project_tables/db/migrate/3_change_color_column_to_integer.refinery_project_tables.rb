# This migration comes from refinery_superinquiries (originally 5)
class ChangeColorColumnToInteger < ActiveRecord::Migration[5.1]
  def up
    change_column :refinery_project_tables, :color, :integer
  end

  def down
    change_column :refinery_project_tables, :color, :string
  end
end
