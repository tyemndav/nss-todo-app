module Refinery
  module ProjectTables
    module Admin
      class ProjectTablesController < ::Refinery::AdminController

        before_action :get_all_users

        crudify :'refinery/project_tables/project_table'

        def get_all_users
          @users = ::Refinery::Authentication::Devise::User.all
        end

        private

        # Only allow a trusted parameter "white list" through.
        def project_table_params
          params.require(:project_table).permit!#(:title, :color, :user_id)
        end
      end
    end
  end
end
