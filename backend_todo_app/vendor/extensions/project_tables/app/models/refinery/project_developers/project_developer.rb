module Refinery
  module ProjectDevelopers
    class ProjectDeveloper < Refinery::Core::BaseModel
      self.table_name = 'refinery_project_developers'

      belongs_to :user, :class_name => '::Refinery::Authentication::Devise::User'
      belongs_to :project_table, :class_name => '::Refinery::ProjectTables::ProjectTable'


      # To enable admin searching, add acts_as_indexed on searchable fields, for example:
      #
      #   acts_as_indexed :fields => [:title]

    end
  end
end
