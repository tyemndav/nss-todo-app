module Refinery
  module ProjectTables
    class ProjectTable < Refinery::Core::BaseModel
      self.table_name = 'refinery_project_tables'

      enum color_enum: ['Grey', 'Yellow', 'Orange', 'Red', 'Purple', 'Blue', 'Green']

      validates :title, :presence => true, :uniqueness => true
      belongs_to :user, :class_name => '::Refinery::Authentication::Devise::User'
      has_many :project_developers, :class_name => '::Refinery::ProjectDevelopers::ProjectDeveloper'
      has_many :users, :class_name => '::Refinery::Authentication::Devise::User', :through => :project_developers
      has_many :card_groups, :class_name => '::Refinery::CardGroups::CardGroup'

      accepts_nested_attributes_for :project_developers, reject_if: :all_blank, allow_destroy: true

      # To enable admin searching, add acts_as_indexed on searchable fields, for example:
      #
      #   acts_as_indexed :fields => [:title]

      def project_color
        ProjectTable.color_enums.keys[self.color]
      end
    end
  end
end
