require 'refinerycms-core'

module Refinery
  autoload :ProjectTablesGenerator, 'generators/refinery/project_tables_generator'

  module ProjectTables
    require 'refinery/project_tables/engine'

    class << self
      attr_writer :root

      def root
        @root ||= Pathname.new(File.expand_path('../../../', __FILE__))
      end

      def factory_paths
        @factory_paths ||= [ root.join('spec', 'factories').to_s ]
      end
    end
  end
end
