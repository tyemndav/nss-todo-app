module Refinery
  module ProjectTables
    class Engine < Rails::Engine
      extend Refinery::Engine
      isolate_namespace Refinery::ProjectTables

      engine_name :refinery_project_tables

      before_inclusion do
        Refinery::Plugin.register do |plugin|
          plugin.name = "project_tables"
          plugin.url = proc { Refinery::Core::Engine.routes.url_helpers.project_tables_admin_project_tables_path }
          plugin.pathname = root
          
        end
      end

      config.after_initialize do
        Refinery.register_extension(Refinery::ProjectTables)
      end
    end
  end
end
