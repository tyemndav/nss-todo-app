module Refinery
  class ProjectTablesGenerator < Rails::Generators::Base

    def rake_db
      rake "refinery_project_tables:install:migrations"
    end

    def append_load_seed_data
      create_file 'db/seeds.rb' unless File.exists?(File.join(destination_root, 'db', 'seeds.rb'))
      append_file 'db/seeds.rb', :verbose => true do
        <<-EOH

# Added by Refinery CMS ProjectTables extension
Refinery::ProjectTables::Engine.load_seed
        EOH
      end
    end
  end
end
