Refinery::Core::Engine.routes.draw do

  # Frontend routes
  namespace :project_tables do
    resources :project_tables, :path => '', :only => [:index, :show] do
      collection do
        post :create
        get :my_own_destroy
      end
      put :update
    end
  end

  # Admin routes
  namespace :project_tables, :path => '' do
    namespace :admin, :path => Refinery::Core.backend_route do
      resources :project_tables, :except => :show do
        collection do
          post :update_positions
        end
      end
    end
  end

end
