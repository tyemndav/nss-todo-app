## Project scope:

Basic todo app like Trello etc. Based on MVC architecture.



## Team members: 

- Michal Štembera
- Lukáš Kotrbatý
- David Tyemnyák
- Martin Sklenár
- Martin Krupa



## Documentation:

https://docs.google.com/document/d/1z1jH1hoHv6XXkBdHMJ7EqEhu2GSu5w-3oVlghW4dNNo/edit?usp=sharing